var BeaconLove = function (data) {

	this.initialize = function(data) {
        console.log('data.phoneID', data.phoneID);
        this.phoneID = data.phoneID;
    };

    //beacon actions
    this.BEACON_LOVE_BACKEND_ACTION_URL = "http://beaconlove.beyond.nazwa.pl/wp-admin/admin-ajax.php";
    this.BEACON_BACKEND_USER_DATA_ACTION = "user_data";
    this.pullUserInfo = function(onsuccess, onerror) {
        onerror = onerror ? onerror : function(){};
        onsuccess = onsuccess ? onsuccess : function(){};
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: lthis.BEACON_LOVE_BACKEND_ACTION_URL,
            data: {
                action: lthis.BEACON_BACKEND_USER_DATA_ACTION,
                phoneID: lthis.phoneID
            },
            success: function(response) {
                console.log("pullUserInfo ajax success!", response);
                onsuccess.call(app, response);
            },
            error: function(response) {
                console.log("pullUserInfo ajax failure...", response);
                onerror.call(app, response);
            },
            dataType: 'json',
            crossDomain: true
         });
    };

    //beacon info
    this.BEACON_BACKEND_BEACONS_DATA_ACTION = "beacons_data";
    this.pullBeaconsInfo = function(onsuccess, onerror) {
        onerror = onerror ? onerror : function(){};
        onsuccess = onsuccess ? onsuccess : function(){};
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: lthis.BEACON_LOVE_BACKEND_ACTION_URL,
            data: {
                action: lthis.BEACON_BACKEND_BEACONS_DATA_ACTION,
                phoneID: lthis.phoneID
            },
            success: function(response) {
                console.log("pullBeaconsInfo ajax success!", response);
                onsuccess.call(app, response);
            },
            error: function(response) {
                console.log("pullBeaconsInfo ajax failure...", response);
                onerror.call(app, response);
            },
            dataType: 'json',
            crossDomain: true
         });
    };

    this.BEACON_BACKEND_ISEE_ICON_ACTION = "isee_beacon";
    this.iSeeBeacon = function(beacon, onsuccess, onerror) {
        onerror = onerror ? onerror : function(){};
        onsuccess = onsuccess ? onsuccess : function(){};
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: lthis.BEACON_LOVE_BACKEND_ACTION_URL,
            data: {
                action: lthis.BEACON_BACKEND_ISEE_ICON_ACTION,
                uuid: beacon.uuid,
                major: beacon.major,
                minor: beacon.minor,
                phoneID: lthis.phoneID
            },
            success: function(response) {
                console.log("iSeeBeacon ajax success!", response);
                onsuccess.call(app, response);
            },
            error: function(response) {
                console.log("iSeeBeacon ajax failure...", response);
                onerror.call(app, response);
            },
            dataType: 'json',
            crossDomain: true
         });
    };

    this.BEACON_BACKEND_REMOVE_TOKENS_ACTION = "remove_all_tokens";
    lthis = this;
    this.removeTokens = function(onsuccess, onerror) {
        onerror = onerror ? onerror : function(){};
        onsuccess = onsuccess ? onsuccess : function(){};
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: lthis.BEACON_LOVE_BACKEND_ACTION_URL,
            data: {
                action: lthis.BEACON_BACKEND_REMOVE_TOKENS_ACTION,
                phoneID: lthis.phoneID
            },
            success: function(response) {
                console.log("removeTokens ajax success!", response);
                onsuccess.call(app, response);
            },
            error: function(response) {
                console.log("removeTokens ajax failure...", response);
                onerror.call(app, response);
            },
            dataType: 'json',
            crossDomain: true
        });
    };

    this.BEACON_BACKEND_REMOVE_TOKEN_ACTION = "remove_token";
    //for tests only
    this.removeToken = function(tokenID, onsuccess, onerror) {
        onerror = onerror ? onerror : function(){};
        onsuccess = onsuccess ? onsuccess : function(){};
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: lthis.BEACON_LOVE_BACKEND_ACTION_URL,
            data: {
                action: lthis.BEACON_BACKEND_REMOVE_TOKEN_ACTION,
                phoneID: lthis.phoneID,
                tokenID: tokenID
            },
            success: function(response) {
                console.log("removeTokens ajax success!", response);
                onsuccess.call(app, response);
            },
            error: function(response) {
                console.log("removeTokens ajax failure...", response);
                onerror.call(app, response);
            },
            dataType: 'json',
            crossDomain: true
        });
    };


    this.initialize(data);
}



