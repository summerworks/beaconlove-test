function BeaconManager() {
    var listeners = {
        'added': [],
        'removed': [],
        'updated': []
    };
    var beacons = {};
    var absentBeacons = {};

    function beaconHash(beacon) {
        if(beacon && beacon.proximityUUID){
            return beacon.proximityUUID + ':' + beacon.major + '-' + beacon.minor;
        } else {
            return false;
        }
    };

    function addBeacon(beacon) {
       var hash = beaconHash(beacon);
       beacons[hash] = beacon;
    }

    function getBeacon(beacon, beacons2) {
         return beacons2[beaconHash(beacon)];
    }

    function compareBeacons(a, b) {
        return beaconHash(a) == beaconHash(b);
    }

    function beaconsHashTable(newBeacons) {
        var i,l;
        var beacons2 = {};
        for(i = 0, l = newBeacons.length; i < l; i++) {
            var b = newBeacons[i];
            var hash = beaconHash(b);
            beacons2[hash] = b;
        }
        return beacons2;

    };

    function updateBeacon(b, newBeacon) {
        b.minor = newBeacon.minor;
        b.major = newBeacon.major;
        b.rssi = newBeacon.rssi;
        b.distance = newBeacon.distance;
        b.isConnected = newBeacon.isConnected;
        b.uuid = newBeacon.proximityUUID;
        b.proximity = newBeacon.proximity;
    }

    function compareBeacons(b1, b2) {

        var MAX_DISTANCE_DIFFERENCE = 1;
        if(Math.abs(b1.distance - b2.distance) > MAX_DISTANCE_DIFFERENCE) {
            return false;
        }

        if(b1.proximity != b2.proximity) {
            return false;
        }

        if(b1.isConnected != b2.isConnected) {
            return false;
        }

        var MAX_RSSI_DIFFERENCE = 10;
        if(Math.abs(b1.rssi - b2.rssi) > MAX_RSSI_DIFFERENCE) {
            return false;
        }

        if(beaconHash(b1) != beaconHash(b2)) {
            return false;
        }

        return true;
    }

    function duplicateBeacon(newBeacon) {
        var b = {};
        updateBeacon(b, newBeacon);
        return b;
    }

    function updateBeacons(newBeacons) {
        //console.log(newBeacons);
        var i,l;
        var beacons2 = beaconsHashTable(newBeacons);
        //console.log('beacons2', beacons2);
        for( var hash in beacons ){
            if(beacons.hasOwnProperty(hash)){
                var b = beacons[hash];
                var MAX_ABSENT = 3;
                var newBeacon = getBeacon(b, beacons2);
                //console.log("newBeacon", hash, newBeacon, absentBeacons);
                if(!newBeacon) {
                    absentBeacons[hash] = absentBeacons[hash] ? absentBeacons[hash]+1 : 1;
                    if(absentBeacons[hash] > MAX_ABSENT) {
                         trigger('removed', duplicateBeacon(b));
                         delete beacons[hash];
                         absentBeacons[hash] = 1;

                    }
                } else {
                    if(!compareBeacons(b, newBeacon)) {
                        updateBeacon(b, newBeacon);
                        trigger('updated', duplicateBeacon(b));
                    }
                }
            }
        };

        for(var hash in beacons2){
            if(beacons2.hasOwnProperty(hash)){
                var b2 = beacons2[hash];
                if(!getBeacon(b2, beacons)) {
                    beacons[hash] = b2;
                    trigger('added', duplicateBeacon(b2));
                }
            }
        };
    }

    function trigger(event, data) {
        for(var i= 0, l=listeners[event].length; i<l; i++) {
            listeners[event][i](data);
        }
    }

    this.getBeacons = function() {
        return beacons;
    };

    this.estimoteUpdate = function() {
        window.EstimoteBeacons.getBeacons(function (beacons) {
            updateBeacons(beacons);
        });
    }

    this.startPulling = function(interval) {
        interval = interval || 1000;

        if(typeof interval !== "number" || isNaN(interval)) {
            throw "Interval must be a valid number.";
        }

        if(interval <= 0) {
            throw "Interval must be a positive number."
        }

        var lthis = this;
        window.EstimoteBeacons.startRangingBeaconsInRegion(function () {
            lthis.pullingInterval = setInterval(function () {
                lthis.estimoteUpdate();
            }, interval);
        });
    };

    this.stopPulling = function() {
        clearInterval(this.pullingInterval);
        window.EstimoteBeacons.stopRangingBeaconsInRegion(
        function() {
            console.log("stopped estimote raging");
        });
    };

    this.on = function(event, callback) {
        if(!listeners[event]) {
            throw "Unknown event '" + event + "'";
        }

        if(typeof callback !== "function") {
            throw "Callback must be a function.";
        }

        listeners[event].push(callback);
    };
}