var BeaconLove = function () {

	this.beacons = {};
	this.tokens = {};

	this.initialize = function(data) {};

	this.addBeacon = function() {};
	this.removeBeacon = function() {};
	this.updateBeacon = function() {};
	this.getBeacons = function() {};
	this.getTokens = function() {};

	this.onBeacon = function(data, callback) {
		window.plugin.notification.local.add({ message: 'Great app!'+ data.minor });
	};
}


var Beacon = function() {

	this.initialize = function(data){};
	this.setLocation = function(location){};
	this.setUUID = function(uuid){};
	this.getActions = function() {};
	this.getTokens = function() {};

}

var Action = function() {

	var triggerTokenAction = function(token){};
	var tiggerStandaloneAction = function(url){};

	this.initialize = function(data) {};
	this.setType = function(actionType){};

	this.triggerAction = function(params) {
		if(this.actionType == Action.STANDALONE_ACTION) {
			tiggerStandaloneAction.call(this, params);	
		} else if(this.actionType == Action.TOKEN_ACTION) {
			triggerTokenAction.call(this, params);
		} else {
			//do nothing
		}
	}
}

Action.prototype.TOKEN_ACTION = 'TOKEN_ACTION';
Action.prototype.STANDALONE_ACTION = 'STANDALONE_ACTION';

var Token = function () {
	this.initialize = function(data) {};
	this.getType = function() {};
	this.getAction = function() {};
}



