/*
 */
(function($) {
    window.app = {
        BEACON_LOVE_BACKEND_URL: 'http://beaconlove.beyond.nazwa.pl/wp_api/v1',
        BEACON_LOVE_POSTS_PATH: '/posts',
        BEACON_LOVE_BEACONS_PARAMS: 'post_type=beacon',
        BEACON_LOVE_ACTIONS_PARAMS: 'post_type=action',
        BEACON_LOVE_TOKEN_PARAMS: 'post_type=tokens',
        // Application Constructor
        initialize: function() {
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function() {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app.receivedEvent(...);'
        onDeviceReady: function() {
            //app.getBeaconsData(function(r){alert(r)});
            alert("deviceready");
            $('.violet-beacon').click(function() {
                app.simulateBeacon("violet");
            });      
            $('.blue-beacon').click(function() {
                app.simulateBeacon("blue");
            });      
            alert(window.plugin); 
            alert("A");
            window.plugin.notification.local.add({ message: 'Great app!' });
        },
        getBeaconsData: function(onsuccess, onerror) {
            $.ajax({
                type: "GET",
                crossDomain: true,
                url: app.BEACON_LOVE_BACKEND_URL + app.BEACON_LOVE_POSTS_PATH,
                data: app.BEACON_LOVE_BEACONS_PARAMS,
                success: function(response) {
                    alert("ajax success!");
                    onsuccess.call(app, response);
                },
                error: function(msg) {
                    onerror.call(app, msg);
                },
                dataType: 'json'
            }); 
        },
        updateInfo: function(response) {
        },
        simulateBeacon: function(color) {
            var blueBeacon = {
                UUID: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
                minor: "18268",
                major: "36609"
            };
            var violetBeacon = {
                UUID: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
                minor: "39293",
                major: "26618"
            };
            alert(color);

            if(color=="blue") {
                setTimeout( function(){
                    //app.beaconLove.onBeacon(blueBeacon);
                    alert("1blue");
                }, 5000 );
            } else if (color="violet") {
                setTimeout( function(){
                    //app.beaconLove.onBeacon(violetBeacon);
                    alert("1violet");
                }, 5000 );
            }
        }
    };
})(jQuery);