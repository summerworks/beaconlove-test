cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.oracle.mx.ux.cordova.estimotebeacons/www/EstimoteBeacons.js",
        "id": "com.oracle.mx.ux.cordova.estimotebeacons.EstimoteBeacons",
        "clobbers": [
            "EstimoteBeacons"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/www/local-notification.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification",
        "clobbers": [
            "plugin.notification.local"
        ]
    },
    {
        "file": "plugins/pl.makingwaves.estimotebeacons/www/EstimoteBeacons.js",
        "id": "pl.makingwaves.estimotebeacons.EstimoteBeacons",
        "clobbers": [
            "EstimoteBeacons"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.background-mode/www/background-mode.js",
        "id": "de.appplant.cordova.plugin.background-mode.BackgroundMode",
        "clobbers": [
            "plugin.backgroundMode"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.badge/www/badge.js",
        "id": "de.appplant.cordova.plugin.badge.Badge",
        "clobbers": [
            "plugin.notification.badge",
            "cordova.plugins.notification.badge"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.oracle.mx.ux.cordova.estimotebeacons": "0.0.3",
    "de.appplant.cordova.plugin.local-notification": "0.7.4",
    "pl.makingwaves.estimotebeacons": "0.1.0",
    "de.appplant.cordova.plugin.background-mode": "0.5.0",
    "de.appplant.cordova.plugin.badge": "0.5.3",
    "org.apache.cordova.console": "0.2.9",
    "org.apache.cordova.device": "0.2.10"
}
// BOTTOM OF METADATA
});