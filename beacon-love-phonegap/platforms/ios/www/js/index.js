/*
 */
(function($) {
    window.app = {
        // Application Constructor
        initialize: function() {
            this.bindEvents();
            this.notNo = 0;
            this.inBackground = false;
            app.NOTIFICATION_ID = '12344445';
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function() {
            document.addEventListener('deviceready', app.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app.receivedEvent(...);'
        onDeviceReady: function() {
            console.log("deviceready");

            this.phoneID = window.device ? window.device.uuid : "1234";
            app.beaconLove = new BeaconLove({phoneID: this.phoneID});

            app.initBeaconManager();
            app.inBackground = false;

            document.addEventListener('resume', app.onResume, false);
            document.addEventListener('resign', app.onResign, false);
            document.addEventListener('pause', app.onPause, false);

            app.clearBadges();

            app.updateUserInfo();
            /// XXX: why doesn't it want to go to background first time? IDN.
            //window.goBackgroundDamnitInterval = setInterval( function() {
            //    window.plugin.backgroundMode.enable();
            //}, 5000 );

        },
        onResume: function(){
            app.inBackground = false;
            app.clearBadges();
            // XXX: stop the hack
            //clearInterval(window.goBackgroundDamnitInterval)
            window.plugin.backgroundMode.enable();
            console.log("resume");
        },
        onResign: function() {
            //window.plugin.backgroundMode.enable();
            app.inBackground = true;
            //console.log("resign 2", app);
        },
        onPause: function() {
            app.inBackground = true;
            //window.plugin.backgroundMode.enable();
            //console.log("pause", app);
        },
        //beacons
        initBeaconManager: function(id) {
            app.beaconManager = new BeaconManager();
            app.beaconManager.startPulling(4000);
            app.beaconManager.on('updated', function(beacon){
                app.updateBeaconInfo(beacon);
                app.beaconLove.iSeeBeacon(
                    beacon,
                    function(response) { //onsuccess
                        var tokens = response.data.tokens;
                        app.addTokenes(tokens);
                        app.updateBeaconInfo(response.data.beacon);

                    },
                    function() {

                    } //onerror
                 );

            });
            app.beaconManager.on('added', function(beacon) {
                app.updateBeaconInfo(beacon);
                app.beaconLove.iSeeBeacon(
                    beacon,
                    function(response) { //onsuccess
                        var tokens = response.data.tokens;
                        var beacon = response.data.beacon;
                        app.addTokenes(tokens);
                    },
                    function() {

                    } //onerror
                 );

            });
            app.beaconManager.on('removed', function(beacon) {
                app.updateBeaconInfo(beacon, true);
                //console.log("removed", beacon);
            });

            window.plugin.notification.local.onclick( function(id, state, json) {
                if(id == app.NOTIFICATION_ID) {
                    app.clearBadges();
                }
            });
            window.plugin.notification.local.oncancel( function(id, state, json) {
                if(id == app.NOTIFICATION_ID) {
                    app.clearBadges();
                }
            });
        },
        clearBadges: function() {
            window.cordova.plugins.notification.badge.clear();
            app.notNo = 0;
        },
        updateUserInfo: function() {
            app.beaconLove.pullUserInfo(
                function(response) { //onsuccess
                    var tokens = response.data.user.tokens;
                    app.addTokenes(tokens);
                },
                function() {

                } //onerror
            );
        },
        addMsg: function(msg) {
            $(".msg").html(msg);
        },
        updateCounts: function() {
            var tokenCount = $('#token-list li').length;
            $('#token-count').html(tokenCount);
            var beaconCount = $('#beacon-list li').length;
            $('#beacon-count').html(beaconCount);

        },
        addTokenes: function(tokens) {
            for(var i in tokens) {
                if(tokens.hasOwnProperty(i)) {
                    var token = tokens[i];
                    msg = "You've got new token!:" + ' : ' + token.title;

                    app.updateTokenInfo(token);
                    app.notNo = app.notNo +1;
                    if(app.inBackground) {
                        window.plugin.notification.local.add({
                            id: app.NOTIFICATION_ID,
                            message: msg,
                            badge: app.notNo,
                            autoCancel: true
                        });
                    }
                    app.addMsg(msg);
                    app.updateCounts();

                } //if

            }

        },
        clearTokens: function () {
            $('#token-list').empty();
            app.addMsg('');
            app.updateCounts();
        },
        //helpers
        generatBeaconTagID: function(beacon) {
            return 'beacon-' + beacon.major + '-' + beacon.minor;
        },
        updateBeaconInfo : function(beacon, remove) {
            //console.log(beacon);
            var beaconList = $('#beacon-list');
            var beaconId = beacon.uuid;
            var beaconMinor = beacon.minor;
            var beaconMajor = beacon.major;
            var beaconProximity = beacon.proximity;
            var beaconDistance = beacon.distance;
            var beaconTitle = beacon.title;
            var beaconTagID = app.generatBeaconTagID(beacon);
            var theNode = $('#' + beaconTagID, beaconList);

            if(remove) {
                theNode.remove();
            } else {
               if(theNode.length == 0) {
                   var content = "<li id='" + beaconTagID + "'>" +
                        "<a href='#' class='ui-btn ui-btn-icon-right ui-icon-carat-r'>" +
                        "Beacon <br/>" +
                        " title: <span class='beacon-title'>" + (beaconTitle ? beaconTitle : '') + "</span>" +
                        " <br/>major: " + beaconMajor +
                        " <br/>minor: " + beaconMinor +
                        " <br/>proximity: <span class='beacon-proximity'>" + beaconProximity + "</span>" +
                        " <br/>distance: <span class='beacon-distance'>" + beaconDistance + "</span>" +
                        "</a></li>";
                    beaconList.append( content );
                } else {
                    if(beaconProximity) {
                        $('.beacon-proximity', theNode).html(beaconProximity);
                    }
                    if(beaconDistance) {
                        $('.beacon-distance', theNode).html(beaconDistance);
                    }
                    if(beaconTitle) {
                        $('.beacon-title', theNode).html(beaconTitle);
                    }

                }
            }
            app.updateCounts();

        },
        updateTokenInfo : function(token, remove) {
           var tokenList = $('#token-list');
           var tokenID = token.ID;
           var action_html = token.action_html;
           var tokenTagID = 'token-' + tokenID;
           var theNode = $('#' + tokenTagID);

           if(remove) {
                theNode.remove();
            } else {
               if(theNode.length == 0) {
                   var content = "<li id='" + tokenTagID + "' class='token-element'>" +
                        "<a href='#'  class='ui-btn ui-btn-icon-right ui-icon-carat-r'>" +
                        "Token" +
                        " ID: " + tokenID +
                        " <div class='action_html'>" + action_html + "</div>" +
                        "<a class='' onClick='app.removeToken(" + tokenID + ")'> Remove </a>"
                        "</a></li>";
                    tokenList.append( content );
                } else {
                    $('.action_html', theNode).html(action_html);
                }
            }
            app.updateCounts();
        },
        removeToken: function(tokenID) {
            app.beaconLove.removeToken(tokenID, function(response) { //success
                app.updateTokenInfo(response.data.token, true);
            },
            function() { //failure

            });
        }
     }; //app
 })(jQuery);